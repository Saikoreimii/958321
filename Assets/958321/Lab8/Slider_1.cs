using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slider_1 : MonoBehaviour
{
    [SerializeField] private Slider sd1;
    [SerializeField] private Slider sd2;
    [SerializeField] private Animator am;
    public string name;
    void Start()
    {
        am = GetComponent<Animator>();
        sd1 = GetComponent<Slider>();
        sd2 = GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        am.SetFloat("Turn",sd1.value);
        am.SetFloat("Forward",sd2.value);
    }
    
    
}
