using UnityEngine;

namespace _958321.Lab4.Script
{
    public class SimpleHealthPointComponent : MonoBehaviour
    {
        [SerializeField] public const float MAX_HP = 100;

        [SerializeField] private float m_HealthPoint;

        //Property
        public float HealthPoint
        {
            get { return m_HealthPoint; }
            set
            {
                m_HealthPoint = value;
                // if(value > 0)
                // {
                // if(value <= MAX_HP)
                // {
                // m_HealthPoint = value;
                // }else{
                // m_HealthPoint = MAX_HP;
                // }
                // }
            }
        }

    }
}

