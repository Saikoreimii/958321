using _958321.Lab3.Script;
using UnityEngine;
namespace _958321.Lab4.Script
 {
     public class PlayerTriggerWithITC : MonoBehaviour
     {
         private void OnTriggerEnter(Collider other)
         {
             //Get components from item object
             //Get the ItemTypeComponent component from the triggered object
             ItemTypeComponent itc = other.GetComponent<ItemTypeComponent>();

             //Get components from the player
             //Inventory
             var inventory = GetComponent<Inventory>();
             //SimpleHealthPointComponent
             var simpleHP = GetComponent<SimpleHealthPointComponent>();
             var PlayerMovement = GetComponent<CapsulePlayerController>();
             if (itc != null)
             {
                 switch (itc.Type)
                 {
                     case ItemType.COIN:
                         inventory.AddItem("COIN",1);
                         break;
                     case ItemType.BIGCOIN:
                         inventory.AddItem("BIGCOIN",1);
                         break;
                     case ItemType.POWERUP:
                         if (simpleHP != null)
                             simpleHP.HealthPoint = simpleHP.HealthPoint + 10;
                         break;
                     case ItemType.POWERDOWN:
                         if (simpleHP != null)
                             simpleHP.HealthPoint = simpleHP.HealthPoint -
                         10;
                         break;
                     case ItemType.SPEEDUP:
                         if (PlayerMovement != null)
                             PlayerMovement.MDirectionalSpeed = 10.0f;
                         break;
                     case ItemType.SPEEDDOWN:
                         if (PlayerMovement != null)
                             PlayerMovement.MDirectionalSpeed = 1.0f;
                         break;
                 }
             }
             Destroy(other.gameObject, 0);
         }
     }
 }
