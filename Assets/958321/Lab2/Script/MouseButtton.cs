using UnityEngine;
using UnityEngine.InputSystem;

namespace _958321.Lab2.Script
{
    public class MouseButtton : StepMovement
    {
        void Update()
        {

            if (Input.GetMouseButtonUp(0))
            {
                MoveUp();
            }
            else if (Input.GetMouseButtonDown(0))
            {
                MoveRight();
            }
        }
    }
}