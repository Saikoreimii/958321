using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _958321.Lab2.Script
{
    public class ControlObjectMovementOnXZPlaneUsingArrowKeys : StepMovement
    {


        // Update is called once per frame
        void Update()
        {

            //GetKey
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                MoveRight();
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                MoveForward();
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                MoveBackward();
            }

            if (Input.GetMouseButtonUp(0))
            {
                MoveLeft();
            }
            else if (Input.GetMouseButtonDown(0))
            {
                MoveRight();
            }
        }
    }
}

